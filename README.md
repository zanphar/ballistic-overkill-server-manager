# Ballistic Overkill Server Manager #


##Introduction:##

Ballistic Overkill Server Manager ("BOSM") is a simple program that is designed to assist, and help you with setting up a Ballistic Overkill Dedicated Server ("BODS"). With this software, you can have your very own server setup in less time, and have more time for playing.

You can find out more about this software, including news and updates at:

* http://chware.net/ballistic-overkill-server-manager/

The source code is available directly from, including additional resources:

* https://bitbucket.org/zanphar/ballistic-overkill-server-manager.

You will need to open port(s) on your firewall / router to allow connections to your Ballistic Overkill dedicated server. For more information, please refer to:

* http://www.portforward.com/

Below is a sample picture of what BOSM looks like right now (December 24, 2015).

![bosm-sample.png](https://bitbucket.org/repo/zbxoM6/images/362029682-bosm-sample.png)



## Key Features: ##
* Download, extract and install SteamCMD much easier and faster.
* Download and install Ballistic Overkill Dedicated Server software.
* Start, stop and restart your server from one single location.
* Configure your server at any time to suit your needs.
* Licensed under the MIT license agreement, it's open source!

## Requirements: ##
* AMD® or Intel® based computer processor.
* Microsoft .NET Framework 4 and Microsoft .NET Framework 4.5.
* Microsoft Windows 7; Windows 8; Windows 8.1 or Windows 10.
* 800x600 minimum screen display; anything less just won't work.
* An active connection to the internet is required to check for, and to download updates, as well as to download additional components such as SteamCMD and Ballistic Overkill Server software.

## Additional Information: ##
We're planning on releasing BOSM on December 24th, 2015 on or before midnight, as a gift to everyone who plays Ballistic Overkill, and wishes to operate their own server.

There is a possibility that it may be released on December 25th, 2015.

Please note the following distribution types that are, or may be made available:

* Installation based version that contains only the binaries, nothing else.
* Portable version that contains only the binary file(s).
* Source package, where the complete source is available in ZIP format.
* Bundle version that includes the installer, portable version and source.

** Please note that not all versions may be available at the date of planned release. **