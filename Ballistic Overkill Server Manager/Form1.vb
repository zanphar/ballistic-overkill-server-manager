﻿' Ballistic Overkill Server Manager Software License Agreement
' Copyright © 2015 Charles M. McDonald
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this
' software and associated documentation files (the “Software”), to deal in the Software
' without restriction, including without limitation the rights to use, copy, modify,
' merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
' permit persons to whom the Software is furnished to do so, subject to the following
' conditions:
'
' The above copyright notice and this permission notice shall be included in all copies
' or substantial portions of the Software.
'
' THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
' INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
' PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
' HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
' OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
' SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Public Class frmMain
    'Just a test comment
    Dim sysDrive As String = Mid(Environment.GetFolderPath(Environment.SpecialFolder.System),
                                   1, 3)
    Dim insDirectory = "SteamCMD"
    Dim insServer = "BallisticOverkill"
    Dim insPath = sysDrive & insDirectory 'Not to modify insDirectory

    Public Sub New()
        InitializeComponent()
        ToolTip1.SetToolTip(btnBrowse, insPath) 'Default path
    End Sub

    Public Shared Sub ExtractToDirectory(sourceArchiveFileName As String, destinationDirectoryName As String)
    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        Try
            Dim folderBrDlg As New FolderBrowserDialog
            folderBrDlg.ShowNewFolderButton = True
            If (folderBrDlg.ShowDialog() = DialogResult.OK) Then
                insPath = folderBrDlg.SelectedPath
                ToolTip1.SetToolTip(btnBrowse, insPath) 'Selected path
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnInstall_Click(sender As Object, e As EventArgs) Handles btnInstall.Click
        Try

            If My.Computer.FileSystem.FileExists(insPath & "\steamcmd.exe") Then 'replaced sysDrive & insDirectory by insPath
                GetBOServer()
            Else
                My.Computer.Network.DownloadFile(
                       "https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip",
                    My.Computer.FileSystem.CurrentDirectory & "\steamcmd.zip", False, 800)
                InstallSteamCMD()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub

    Sub InstallSteamCMD()
        Try
            Dim zipPath As String = My.Computer.FileSystem.CurrentDirectory & "\steamcmd.zip"
            'Dim extractPath As String = sysDrive & insDirectory
            ZipFile.ExtractToDirectory(zipPath, insPath) 'Replaced extractPath by insPath
            GetBOServer()
            My.Settings.installPath = insPath 'Save the path for when uninstall, should check if server's really installed
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub GetBOServer()
        'replaced sysDrive & insDirectory by insPath
        Try
            If My.Settings.installPath = "" Then
                If chkClose.Checked = True Then
                    Dim startInfo As New ProcessStartInfo
                    startInfo.FileName = insPath & "\steamcmd"
                    startInfo.Arguments = "+login anonymous +force_install_dir " & insPath & "\" & insServer & " +app_update 416880 validate +quit"
                    Process.Start(startInfo)
                Else
                    Dim startInfo As New ProcessStartInfo
                    startInfo.FileName = insPath & "\steamcmd"
                    startInfo.Arguments = "+login anonymous +force_install_dir " & insPath & "\" & insServer & " +app_update 416880 validate"
                    Process.Start(startInfo)
                End If
            Else
                MsgBox("A Server is already installed here: " & My.Settings.installPath & "\" & insServer, MsgBoxStyle.Exclamation)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnPreview_Click(sender As Object, e As EventArgs) Handles btnPreview.Click
        Try
            If txtPicURL.Text = "" Or txtURL.Text = "" Then
                MsgBox("Sorry, but you can't request a preview if there is nothing to view. The Banner URL and/or Web Site URL can't be empty. Please try again." & vbCrLf & vbCrLf & _
                       "If this problem presists, please contact the developer.", MsgBoxStyle.Exclamation)
            Else
                picBannerPreview.Navigate(txtPicURL.Text)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub DownloadFile(address As Uri, fileName As String)
    End Sub

    Public Sub Delete(path As String)
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub

    Private Sub tooBOonSteam_Click(sender As Object, e As EventArgs) Handles tooBOonSteam.Click
        Try
            Process.Start("http://store.steampowered.com/app/296300/")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub tooEULA_Click(sender As Object, e As EventArgs) Handles tooEULA.Click
        Try
            Me.Hide()
            Form2.ShowDialog()
            Me.Show()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub tooZanphar_Click(sender As Object, e As EventArgs) Handles tooZanphar.Click
        Try
            Process.Start("https://www.facebook.com/Zanphar")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub lnkLicense_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkLicense.LinkClicked
        Try
            Me.Hide()
            Form2.ShowDialog()
            Me.Show()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnRemoveBODS_Click(sender As Object, e As EventArgs) Handles btnRemoveBODS.Click
        Try
            If My.Settings.installPath <> "" Then
                System.IO.Directory.Delete(My.Settings.installPath & "\" & insServer, True) 'replaced sysDrive & insDirectory by the saved InstallPath
                My.Settings.installPath = "" 'reset
                MsgBox("Server successfully removed.", MsgBoxStyle.Exclamation)
            Else
                MsgBox("No previously installed Server.", MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnStartServer_Click(sender As Object, e As EventArgs) Handles btnStartServer.Click
        Try
            If System.IO.File.Exists(sysDrive & insDirectory & "\" & insServer & "\BODSLauncher.exe") = True Then
                Process.Start(sysDrive & insDirectory & "\" & insServer & "\BODSLauncher.exe")
            Else
                MsgBox("The server has not been installed or is missing from the prescribed directory.", MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btnStopServer_Click(sender As Object, e As EventArgs) Handles btnStopServer.Click
        Try
            MsgBox("This feature is currently not implemented.", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub tooExit_Click(sender As Object, e As EventArgs) Handles tooExit.Click
        Application.Exit()
    End Sub


End Class
