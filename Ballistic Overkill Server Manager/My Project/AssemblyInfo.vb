﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Ballistic Overkill Server Manager")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("CharlieWARE SOFTWARE")> 
<Assembly: AssemblyProduct("Ballistic Overkill Server Manager")> 
<Assembly: AssemblyCopyright("Copyright © 2015 Charles McDonald")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("dfa8cc99-816b-4466-81a8-66126417e6d0")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.*")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
