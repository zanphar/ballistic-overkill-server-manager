﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblServerName = New System.Windows.Forms.Label()
        Me.lblServerPort = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tooExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tooBOonSteam = New System.Windows.Forms.ToolStripMenuItem()
        Me.tooZanphar = New System.Windows.Forms.ToolStripMenuItem()
        Me.tooEULA = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.txtServerName = New System.Windows.Forms.TextBox()
        Me.txtPort = New System.Windows.Forms.TextBox()
        Me.lblMaxPlayers = New System.Windows.Forms.Label()
        Me.comMaxPlayers = New System.Windows.Forms.ComboBox()
        Me.txtMaxPing = New System.Windows.Forms.TextBox()
        Me.lblMaxPing = New System.Windows.Forms.Label()
        Me.comStartMap = New System.Windows.Forms.ComboBox()
        Me.lblStartMap = New System.Windows.Forms.Label()
        Me.comGameMode = New System.Windows.Forms.ComboBox()
        Me.lblGameMode = New System.Windows.Forms.Label()
        Me.txtURL = New System.Windows.Forms.TextBox()
        Me.txtPicURL = New System.Windows.Forms.TextBox()
        Me.lblDevURL = New System.Windows.Forms.Label()
        Me.lblBannerURL = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnInstall = New System.Windows.Forms.Button()
        Me.btnStartServer = New System.Windows.Forms.Button()
        Me.btnStopServer = New System.Windows.Forms.Button()
        Me.btnRemoveBODS = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.picBannerPreview = New System.Windows.Forms.WebBrowser()
        Me.lblServerNameNote = New System.Windows.Forms.Label()
        Me.lblServerPortNote = New System.Windows.Forms.Label()
        Me.lblMaxPlayersNote = New System.Windows.Forms.Label()
        Me.tabCmds = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnRestore = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnBackup = New System.Windows.Forms.Button()
        Me.chkClose = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lnkDevPage = New System.Windows.Forms.LinkLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblPreviewNotre = New System.Windows.Forms.Label()
        Me.lblPingNote = New System.Windows.Forms.Label()
        Me.lblStartingMapNote = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.statStr = New System.Windows.Forms.StatusStrip()
        Me.tooProgress = New System.Windows.Forms.ToolStripProgressBar()
        Me.tooDateTime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lnkLicense = New System.Windows.Forms.LinkLabel()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1.SuspendLayout()
        Me.tabCmds.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.statStr.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblServerName
        '
        Me.lblServerName.ForeColor = System.Drawing.Color.DarkRed
        Me.lblServerName.Location = New System.Drawing.Point(18, 186)
        Me.lblServerName.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblServerName.Name = "lblServerName"
        Me.lblServerName.Size = New System.Drawing.Size(200, 46)
        Me.lblServerName.TabIndex = 0
        Me.lblServerName.Text = "Server Name:"
        Me.lblServerName.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblServerPort
        '
        Me.lblServerPort.ForeColor = System.Drawing.Color.DarkRed
        Me.lblServerPort.Location = New System.Drawing.Point(18, 316)
        Me.lblServerPort.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblServerPort.Name = "lblServerPort"
        Me.lblServerPort.Size = New System.Drawing.Size(200, 46)
        Me.lblServerPort.TabIndex = 1
        Me.lblServerPort.Text = "Server Port:"
        Me.lblServerPort.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(12, 4, 0, 4)
        Me.MenuStrip1.Size = New System.Drawing.Size(1568, 44)
        Me.MenuStrip1.TabIndex = 8
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tooExit})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(64, 36)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'tooExit
        '
        Me.tooExit.Name = "tooExit"
        Me.tooExit.Size = New System.Drawing.Size(152, 38)
        Me.tooExit.Text = "&Exit"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem, Me.tooBOonSteam, Me.tooZanphar, Me.tooEULA})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(77, 36)
        Me.HelpToolStripMenuItem.Text = "&Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(390, 38)
        Me.AboutToolStripMenuItem.Text = "&About"
        '
        'tooBOonSteam
        '
        Me.tooBOonSteam.Name = "tooBOonSteam"
        Me.tooBOonSteam.Size = New System.Drawing.Size(390, 38)
        Me.tooBOonSteam.Text = "&Ballistic Overkill on Steam"
        '
        'tooZanphar
        '
        Me.tooZanphar.Name = "tooZanphar"
        Me.tooZanphar.Size = New System.Drawing.Size(390, 38)
        Me.tooZanphar.Text = "&Zanphar on Facebook"
        '
        'tooEULA
        '
        Me.tooEULA.Name = "tooEULA"
        Me.tooEULA.Size = New System.Drawing.Size(390, 38)
        Me.tooEULA.Text = "&License Agreement"
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Text = "NotifyIcon1"
        Me.NotifyIcon1.Visible = True
        '
        'txtServerName
        '
        Me.txtServerName.Location = New System.Drawing.Point(236, 186)
        Me.txtServerName.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.txtServerName.Name = "txtServerName"
        Me.txtServerName.Size = New System.Drawing.Size(576, 31)
        Me.txtServerName.TabIndex = 9
        Me.txtServerName.Text = "My Server"
        '
        'txtPort
        '
        Me.txtPort.Location = New System.Drawing.Point(236, 316)
        Me.txtPort.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(576, 31)
        Me.txtPort.TabIndex = 10
        Me.txtPort.Text = "27015"
        '
        'lblMaxPlayers
        '
        Me.lblMaxPlayers.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMaxPlayers.Location = New System.Drawing.Point(18, 452)
        Me.lblMaxPlayers.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblMaxPlayers.Name = "lblMaxPlayers"
        Me.lblMaxPlayers.Size = New System.Drawing.Size(200, 46)
        Me.lblMaxPlayers.TabIndex = 11
        Me.lblMaxPlayers.Text = "Maximum Players:"
        Me.lblMaxPlayers.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'comMaxPlayers
        '
        Me.comMaxPlayers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comMaxPlayers.FormattingEnabled = True
        Me.comMaxPlayers.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"})
        Me.comMaxPlayers.Location = New System.Drawing.Point(236, 452)
        Me.comMaxPlayers.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.comMaxPlayers.Name = "comMaxPlayers"
        Me.comMaxPlayers.Size = New System.Drawing.Size(576, 33)
        Me.comMaxPlayers.TabIndex = 12
        '
        'txtMaxPing
        '
        Me.txtMaxPing.Location = New System.Drawing.Point(236, 672)
        Me.txtMaxPing.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.txtMaxPing.Name = "txtMaxPing"
        Me.txtMaxPing.Size = New System.Drawing.Size(576, 31)
        Me.txtMaxPing.TabIndex = 14
        Me.txtMaxPing.Text = "500"
        '
        'lblMaxPing
        '
        Me.lblMaxPing.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMaxPing.Location = New System.Drawing.Point(18, 672)
        Me.lblMaxPing.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblMaxPing.Name = "lblMaxPing"
        Me.lblMaxPing.Size = New System.Drawing.Size(200, 46)
        Me.lblMaxPing.TabIndex = 13
        Me.lblMaxPing.Text = "Maximum Ping:"
        Me.lblMaxPing.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'comStartMap
        '
        Me.comStartMap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comStartMap.FormattingEnabled = True
        Me.comStartMap.Items.AddRange(New Object() {"# 1: Corporate Park", "# 2: Hollow", "# 3: Sunnsquare Mall", "# 4: Nox Museum", "# 7: Citadel", "# 8: Overhead"})
        Me.comStartMap.Location = New System.Drawing.Point(236, 808)
        Me.comStartMap.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.comStartMap.Name = "comStartMap"
        Me.comStartMap.Size = New System.Drawing.Size(576, 33)
        Me.comStartMap.TabIndex = 16
        '
        'lblStartMap
        '
        Me.lblStartMap.ForeColor = System.Drawing.Color.DarkRed
        Me.lblStartMap.Location = New System.Drawing.Point(18, 808)
        Me.lblStartMap.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblStartMap.Name = "lblStartMap"
        Me.lblStartMap.Size = New System.Drawing.Size(200, 46)
        Me.lblStartMap.TabIndex = 15
        Me.lblStartMap.Text = "Starting Map:"
        Me.lblStartMap.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'comGameMode
        '
        Me.comGameMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comGameMode.FormattingEnabled = True
        Me.comGameMode.Items.AddRange(New Object() {"# 1: Team Deatchmatch", "# 3: King of the Hill", "# 5: Free for all"})
        Me.comGameMode.Location = New System.Drawing.Point(236, 946)
        Me.comGameMode.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.comGameMode.Name = "comGameMode"
        Me.comGameMode.Size = New System.Drawing.Size(576, 33)
        Me.comGameMode.TabIndex = 18
        '
        'lblGameMode
        '
        Me.lblGameMode.ForeColor = System.Drawing.Color.DarkRed
        Me.lblGameMode.Location = New System.Drawing.Point(18, 946)
        Me.lblGameMode.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblGameMode.Name = "lblGameMode"
        Me.lblGameMode.Size = New System.Drawing.Size(200, 46)
        Me.lblGameMode.TabIndex = 17
        Me.lblGameMode.Text = "Game Mode:"
        Me.lblGameMode.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtURL
        '
        Me.txtURL.Location = New System.Drawing.Point(236, 1276)
        Me.txtURL.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.txtURL.Name = "txtURL"
        Me.txtURL.Size = New System.Drawing.Size(576, 31)
        Me.txtURL.TabIndex = 22
        '
        'txtPicURL
        '
        Me.txtPicURL.Location = New System.Drawing.Point(236, 1166)
        Me.txtPicURL.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.txtPicURL.Name = "txtPicURL"
        Me.txtPicURL.Size = New System.Drawing.Size(576, 31)
        Me.txtPicURL.TabIndex = 21
        '
        'lblDevURL
        '
        Me.lblDevURL.Location = New System.Drawing.Point(18, 1276)
        Me.lblDevURL.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblDevURL.Name = "lblDevURL"
        Me.lblDevURL.Size = New System.Drawing.Size(200, 46)
        Me.lblDevURL.TabIndex = 20
        Me.lblDevURL.Text = "Web Site URL:"
        Me.lblDevURL.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblBannerURL
        '
        Me.lblBannerURL.Location = New System.Drawing.Point(18, 1166)
        Me.lblBannerURL.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblBannerURL.Name = "lblBannerURL"
        Me.lblBannerURL.Size = New System.Drawing.Size(200, 46)
        Me.lblBannerURL.TabIndex = 19
        Me.lblBannerURL.Text = "Banner URL:"
        Me.lblBannerURL.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(1340, 724)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(150, 46)
        Me.btnExit.TabIndex = 24
        Me.btnExit.Text = "&Close"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnInstall
        '
        Me.btnInstall.Location = New System.Drawing.Point(308, 58)
        Me.btnInstall.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.btnInstall.Name = "btnInstall"
        Me.btnInstall.Size = New System.Drawing.Size(150, 46)
        Me.btnInstall.TabIndex = 25
        Me.btnInstall.Text = "&Install"
        Me.btnInstall.UseVisualStyleBackColor = True
        '
        'btnStartServer
        '
        Me.btnStartServer.Location = New System.Drawing.Point(308, 12)
        Me.btnStartServer.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.btnStartServer.Name = "btnStartServer"
        Me.btnStartServer.Size = New System.Drawing.Size(150, 46)
        Me.btnStartServer.TabIndex = 26
        Me.btnStartServer.Text = "&Start"
        Me.btnStartServer.UseVisualStyleBackColor = True
        '
        'btnStopServer
        '
        Me.btnStopServer.Location = New System.Drawing.Point(308, 80)
        Me.btnStopServer.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.btnStopServer.Name = "btnStopServer"
        Me.btnStopServer.Size = New System.Drawing.Size(150, 46)
        Me.btnStopServer.TabIndex = 27
        Me.btnStopServer.Text = "S&top"
        Me.btnStopServer.UseVisualStyleBackColor = True
        '
        'btnRemoveBODS
        '
        Me.btnRemoveBODS.Location = New System.Drawing.Point(308, 182)
        Me.btnRemoveBODS.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.btnRemoveBODS.Name = "btnRemoveBODS"
        Me.btnRemoveBODS.Size = New System.Drawing.Size(150, 46)
        Me.btnRemoveBODS.TabIndex = 28
        Me.btnRemoveBODS.Text = "&Remove"
        Me.btnRemoveBODS.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(666, 1218)
        Me.btnPreview.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(150, 46)
        Me.btnPreview.TabIndex = 29
        Me.btnPreview.Text = "&Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'picBannerPreview
        '
        Me.picBannerPreview.AllowNavigation = False
        Me.picBannerPreview.AllowWebBrowserDrop = False
        Me.picBannerPreview.IsWebBrowserContextMenuEnabled = False
        Me.picBannerPreview.Location = New System.Drawing.Point(102, 1414)
        Me.picBannerPreview.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.picBannerPreview.MinimumSize = New System.Drawing.Size(40, 40)
        Me.picBannerPreview.Name = "picBannerPreview"
        Me.picBannerPreview.ScriptErrorsSuppressed = True
        Me.picBannerPreview.ScrollBarsEnabled = False
        Me.picBannerPreview.Size = New System.Drawing.Size(1328, 400)
        Me.picBannerPreview.TabIndex = 30
        Me.picBannerPreview.WebBrowserShortcutsEnabled = False
        '
        'lblServerNameNote
        '
        Me.lblServerNameNote.Location = New System.Drawing.Point(18, 118)
        Me.lblServerNameNote.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblServerNameNote.Name = "lblServerNameNote"
        Me.lblServerNameNote.Size = New System.Drawing.Size(994, 68)
        Me.lblServerNameNote.TabIndex = 31
        Me.lblServerNameNote.Text = "Please give your server a name that is unique from other server names. This will " &
    "also be used to identify your system from the others that are available." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'lblServerPortNote
        '
        Me.lblServerPortNote.Location = New System.Drawing.Point(18, 248)
        Me.lblServerPortNote.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblServerPortNote.Name = "lblServerPortNote"
        Me.lblServerPortNote.Size = New System.Drawing.Size(994, 68)
        Me.lblServerPortNote.TabIndex = 32
        Me.lblServerPortNote.Text = resources.GetString("lblServerPortNote.Text")
        '
        'lblMaxPlayersNote
        '
        Me.lblMaxPlayersNote.Location = New System.Drawing.Point(18, 378)
        Me.lblMaxPlayersNote.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblMaxPlayersNote.Name = "lblMaxPlayersNote"
        Me.lblMaxPlayersNote.Size = New System.Drawing.Size(994, 68)
        Me.lblMaxPlayersNote.TabIndex = 33
        Me.lblMaxPlayersNote.Text = resources.GetString("lblMaxPlayersNote.Text")
        '
        'tabCmds
        '
        Me.tabCmds.Controls.Add(Me.TabPage1)
        Me.tabCmds.Controls.Add(Me.TabPage2)
        Me.tabCmds.Location = New System.Drawing.Point(1024, 118)
        Me.tabCmds.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.tabCmds.Name = "tabCmds"
        Me.tabCmds.SelectedIndex = 0
        Me.tabCmds.Size = New System.Drawing.Size(486, 594)
        Me.tabCmds.TabIndex = 34
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnSave)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.btnStartServer)
        Me.TabPage1.Controls.Add(Me.btnStopServer)
        Me.TabPage1.Location = New System.Drawing.Point(8, 39)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.TabPage1.Size = New System.Drawing.Size(470, 547)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Commands"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(308, 442)
        Me.btnSave.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(150, 46)
        Me.btnSave.TabIndex = 44
        Me.btnSave.Text = "Sa&ve"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label6.Location = New System.Drawing.Point(18, 348)
        Me.Label6.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(440, 88)
        Me.Label6.TabIndex = 45
        Me.Label6.Text = "When you're satisfied with all the changes you've made, you must save them so the" &
    " server can use them."
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(18, 170)
        Me.Label13.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(440, 86)
        Me.Label13.TabIndex = 30
        Me.Label13.Text = "This will prevent others from accessing the dedicated server software as it will " &
    "no longer be loaded."
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(12, 80)
        Me.Label12.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(284, 52)
        Me.Label12.TabIndex = 29
        Me.Label12.Text = "&Terminate the dedicated server software."
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(12, 12)
        Me.Label11.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(284, 52)
        Me.Label11.TabIndex = 28
        Me.Label11.Text = "&Start the dedicated server software."
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnBrowse)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.btnRestore)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.btnBackup)
        Me.TabPage2.Controls.Add(Me.chkClose)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.btnInstall)
        Me.TabPage2.Controls.Add(Me.btnRemoveBODS)
        Me.TabPage2.Location = New System.Drawing.Point(8, 39)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.TabPage2.Size = New System.Drawing.Size(470, 547)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Resources"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.Enabled = False
        Me.Label8.Location = New System.Drawing.Point(18, 358)
        Me.Label8.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(284, 52)
        Me.Label8.TabIndex = 35
        Me.Label8.Text = "Res&tore server configuration file."
        '
        'btnRestore
        '
        Me.btnRestore.Enabled = False
        Me.btnRestore.Location = New System.Drawing.Point(308, 358)
        Me.btnRestore.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.btnRestore.Name = "btnRestore"
        Me.btnRestore.Size = New System.Drawing.Size(150, 46)
        Me.btnRestore.TabIndex = 34
        Me.btnRestore.Text = "Res&tore"
        Me.btnRestore.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.Enabled = False
        Me.Label7.Location = New System.Drawing.Point(12, 280)
        Me.Label7.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(284, 52)
        Me.Label7.TabIndex = 33
        Me.Label7.Text = "&Backup server configuration file."
        '
        'btnBackup
        '
        Me.btnBackup.Enabled = False
        Me.btnBackup.Location = New System.Drawing.Point(308, 280)
        Me.btnBackup.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.btnBackup.Name = "btnBackup"
        Me.btnBackup.Size = New System.Drawing.Size(150, 46)
        Me.btnBackup.TabIndex = 32
        Me.btnBackup.Text = "&Backup"
        Me.btnBackup.UseVisualStyleBackColor = True
        '
        'chkClose
        '
        Me.chkClose.Location = New System.Drawing.Point(18, 110)
        Me.chkClose.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.chkClose.Name = "chkClose"
        Me.chkClose.Size = New System.Drawing.Size(440, 60)
        Me.chkClose.TabIndex = 31
        Me.chkClose.Text = "Do you wish to close after installation has completed?"
        Me.chkClose.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(12, 182)
        Me.Label5.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(284, 52)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "&Remove the Ballistic Overkill Dedicated Server software."
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(12, 18)
        Me.Label4.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(284, 86)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "&Install SteamCMD and the Ballistic Overkill Dedicated Server software."
        '
        'lnkDevPage
        '
        Me.lnkDevPage.AutoSize = True
        Me.lnkDevPage.Location = New System.Drawing.Point(8, 1936)
        Me.lnkDevPage.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lnkDevPage.Name = "lnkDevPage"
        Me.lnkDevPage.Size = New System.Drawing.Size(398, 25)
        Me.lnkDevPage.TabIndex = 35
        Me.lnkDevPage.TabStop = True
        Me.lnkDevPage.Text = "Copyright © 2015 Charles M. McDonald."
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(14, 1994)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1496, 32)
        Me.Panel1.TabIndex = 36
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(14, 1898)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1496, 32)
        Me.Panel2.TabIndex = 37
        '
        'lblPreviewNotre
        '
        Me.lblPreviewNotre.Location = New System.Drawing.Point(24, 1340)
        Me.lblPreviewNotre.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblPreviewNotre.Name = "lblPreviewNotre"
        Me.lblPreviewNotre.Size = New System.Drawing.Size(1486, 68)
        Me.lblPreviewNotre.TabIndex = 38
        Me.lblPreviewNotre.Text = "The preview will be shown when you have entered in a URL with an image and have p" &
    "ressed &Preview. Please be advised that the image should be no more or less than" &
    " 640x200 pixels." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'lblPingNote
        '
        Me.lblPingNote.Location = New System.Drawing.Point(18, 516)
        Me.lblPingNote.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblPingNote.Name = "lblPingNote"
        Me.lblPingNote.Size = New System.Drawing.Size(994, 134)
        Me.lblPingNote.TabIndex = 39
        Me.lblPingNote.Text = resources.GetString("lblPingNote.Text")
        '
        'lblStartingMapNote
        '
        Me.lblStartingMapNote.Location = New System.Drawing.Point(18, 734)
        Me.lblStartingMapNote.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblStartingMapNote.Name = "lblStartingMapNote"
        Me.lblStartingMapNote.Size = New System.Drawing.Size(994, 68)
        Me.lblStartingMapNote.TabIndex = 40
        Me.lblStartingMapNote.Text = "Select the map that you wish to start off with when starting your server. This ma" &
    "p will also be used every time you start up the server."
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(18, 872)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(994, 68)
        Me.Label1.TabIndex = 41
        Me.Label1.Text = "You can choose the game mode that you prefer such as Team Death Match, Free for A" &
    "ll, and King of the Hill. You may only choose one of the available modes."
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(18, 1078)
        Me.Label2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(1492, 88)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = resources.GetString("Label2.Text")
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(828, 1218)
        Me.Label3.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(662, 62)
        Me.Label3.TabIndex = 43
        Me.Label3.Text = "The preview option allows you to view how your image will appear. Please remember" &
    " that it can only be 640 by 200 pixels."
        '
        'statStr
        '
        Me.statStr.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.statStr.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tooProgress, Me.tooDateTime})
        Me.statStr.Location = New System.Drawing.Point(0, 2026)
        Me.statStr.Name = "statStr"
        Me.statStr.Padding = New System.Windows.Forms.Padding(2, 0, 28, 0)
        Me.statStr.Size = New System.Drawing.Size(1568, 38)
        Me.statStr.SizingGrip = False
        Me.statStr.TabIndex = 7
        Me.statStr.Text = "StatusStrip1"
        '
        'tooProgress
        '
        Me.tooProgress.Name = "tooProgress"
        Me.tooProgress.Size = New System.Drawing.Size(200, 32)
        '
        'tooDateTime
        '
        Me.tooDateTime.Name = "tooDateTime"
        Me.tooDateTime.Size = New System.Drawing.Size(1336, 33)
        Me.tooDateTime.Spring = True
        Me.tooDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lnkLicense
        '
        Me.lnkLicense.Location = New System.Drawing.Point(8, 1962)
        Me.lnkLicense.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lnkLicense.Name = "lnkLicense"
        Me.lnkLicense.Size = New System.Drawing.Size(396, 26)
        Me.lnkLicense.TabIndex = 44
        Me.lnkLicense.TabStop = True
        Me.lnkLicense.Text = "Software licensed under a MIT license."
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(308, 18)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(48, 32)
        Me.btnBrowse.TabIndex = 36
        Me.btnBrowse.Text = "..."
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(192.0!, 192.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1602, 1122)
        Me.Controls.Add(Me.lnkLicense)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.statStr)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblStartingMapNote)
        Me.Controls.Add(Me.lblPingNote)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lblPreviewNotre)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lnkDevPage)
        Me.Controls.Add(Me.tabCmds)
        Me.Controls.Add(Me.lblMaxPlayersNote)
        Me.Controls.Add(Me.lblServerPortNote)
        Me.Controls.Add(Me.lblServerNameNote)
        Me.Controls.Add(Me.picBannerPreview)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.txtURL)
        Me.Controls.Add(Me.txtPicURL)
        Me.Controls.Add(Me.lblDevURL)
        Me.Controls.Add(Me.lblBannerURL)
        Me.Controls.Add(Me.comGameMode)
        Me.Controls.Add(Me.lblGameMode)
        Me.Controls.Add(Me.comStartMap)
        Me.Controls.Add(Me.lblStartMap)
        Me.Controls.Add(Me.txtMaxPing)
        Me.Controls.Add(Me.lblMaxPing)
        Me.Controls.Add(Me.comMaxPlayers)
        Me.Controls.Add(Me.lblMaxPlayers)
        Me.Controls.Add(Me.txtPort)
        Me.Controls.Add(Me.txtServerName)
        Me.Controls.Add(Me.lblServerPort)
        Me.Controls.Add(Me.lblServerName)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ballistic Overkill Server Manager"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.tabCmds.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.statStr.ResumeLayout(False)
        Me.statStr.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblServerName As System.Windows.Forms.Label
    Friend WithEvents lblServerPort As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tooExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents txtServerName As System.Windows.Forms.TextBox
    Friend WithEvents txtPort As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxPlayers As System.Windows.Forms.Label
    Friend WithEvents comMaxPlayers As System.Windows.Forms.ComboBox
    Friend WithEvents txtMaxPing As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxPing As System.Windows.Forms.Label
    Friend WithEvents comStartMap As System.Windows.Forms.ComboBox
    Friend WithEvents lblStartMap As System.Windows.Forms.Label
    Friend WithEvents comGameMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblGameMode As System.Windows.Forms.Label
    Friend WithEvents txtURL As System.Windows.Forms.TextBox
    Friend WithEvents txtPicURL As System.Windows.Forms.TextBox
    Friend WithEvents lblDevURL As System.Windows.Forms.Label
    Friend WithEvents lblBannerURL As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnInstall As System.Windows.Forms.Button
    Friend WithEvents btnStartServer As System.Windows.Forms.Button
    Friend WithEvents btnStopServer As System.Windows.Forms.Button
    Friend WithEvents btnRemoveBODS As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents picBannerPreview As System.Windows.Forms.WebBrowser
    Friend WithEvents lblServerNameNote As System.Windows.Forms.Label
    Friend WithEvents lblServerPortNote As System.Windows.Forms.Label
    Friend WithEvents lblMaxPlayersNote As System.Windows.Forms.Label
    Friend WithEvents tabCmds As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lnkDevPage As System.Windows.Forms.LinkLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblPreviewNotre As System.Windows.Forms.Label
    Friend WithEvents tooBOonSteam As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tooZanphar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblPingNote As System.Windows.Forms.Label
    Friend WithEvents tooEULA As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblStartingMapNote As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents statStr As System.Windows.Forms.StatusStrip
    Friend WithEvents tooProgress As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents tooDateTime As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lnkLicense As System.Windows.Forms.LinkLabel
    Friend WithEvents chkClose As System.Windows.Forms.CheckBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnRestore As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnBackup As System.Windows.Forms.Button
    Friend WithEvents btnBrowse As Button
    Friend WithEvents ToolTip1 As ToolTip
End Class
